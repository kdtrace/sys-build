#!/bin/bash
echo "#############################################";
echo "############# Build Back-End  ###############";
echo "#############################################";
echo "";

cd /root/kdtrace

echo "#############################################";
echo "########## Kéo Master Back-end  ############";
echo "#############################################";
echo "";

git pull

echo "#############################################";
echo "########## Build static file  ###############";
echo "#############################################";
echo "";

mvn clean install 


echo "#############################################";
echo "############ Thêm file jar  ################";
echo "#############################################";
echo "";

docker build -t kdtrace-backend . 

echo "#############################################";
echo "##########  Restart Service   ###############";
echo "#############################################";
echo "";

docker stop kdtrace-backend
docker rm kdtrace-backend
docker run -v /var/www/react-app/build/image-store:/usr/app/image-store -d  --name kdtrace-backend -p 8080:8080 kdtrace-backend

echo "";
echo "#############################################";
echo "######  Deploy Service Successfully   #######";
echo "#############################################";