#!/bin/bash
echo "";
echo "#############################################";
echo "############ Build Front-End  ###############";
echo "#############################################";
echo "";

cd /root/frontend

echo "#############################################";
echo "########## Kéo Master front-end  ############";
echo "#############################################";
echo "";

git pull

echo "#############################################";
echo "########## Build static file  ###############";
echo "#############################################";
echo "";

npm i
npm run build 

echo "#############################################";
echo "########### Nhúng vào NGINX  ################";
echo "#############################################";
echo "";
mv /var/www/react-app/build/image-store /var/www/react-app/
rm -rf /var/www/react-app/build

cp -r /root/frontend/build /var/www/react-app/
mv /var/www/react-app/image-store /var/www/react-app/build/
cp -r /var/www/react-app/.well-known /var/www/react-app/build

echo "#############################################";
echo "##########  Restart Service   ###############";
echo "#############################################";
echo "";

sudo nginx -s stop
sudo nginx

echo "";
echo "#############################################";
echo "######  Deploy Service Successfully   #######";
echo "#############################################";
echo "";
