#!/bin/bash
echo "";
echo "#############################################";
echo "############ Build Front-End  ###############";
echo "#############################################";
echo "";

cd /root/end-user

echo "#############################################";
echo "########## Kéo Master front-end  ############";
echo "#############################################";
echo "";

git pull

echo "#############################################";
echo "########## Build static file  ###############";
echo "#############################################";
echo "";

npm i
npm run build 

echo "#############################################";
echo "########### Nhúng vào NGINX  ################";
echo "#############################################";
echo "";

rm -rf /var/www/react-app-end-user/build

cp -r /root/end-user/build /var/www/react-app-end-user/
cp -r /var/www/react-app-end-user/.well-known /var/www/react-app-end-user/build

echo "#############################################";
echo "##########  Restart Service   ###############";
echo "#############################################";
echo "";

sudo nginx -s stop
sudo nginx

echo "";
echo "#############################################";
echo "######  Deploy Service Successfully   #######";
echo "#############################################";
echo "";
